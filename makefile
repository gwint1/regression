all:	lin_regression

lin_regression:	simple_lin_regression.o
	clang++ simple_lin_regression.o -o lin_regression

simple_lin_regression.o:	simple_lin_regression.cpp
	clang++ -c simple_lin_regression.cpp -std=c++11 -lml -Wextra -Wall -pedantic

clean:
	rm *.o lin_regression
