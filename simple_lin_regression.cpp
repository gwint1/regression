#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <sstream>
#include <cmath>

double get_sum(const std::vector<std::pair<int , int> > &points, int idx,
               int pow)
{
  double sum = 0;

  for(unsigned int i = 0; i < points.size(); i++)
  {
    double value =
         idx == 0 ? (double)points[i].first : (double)points[i].second;
    sum += std::pow(value, (double)pow);
  }

  return sum;
}

double get_sum_of_products(const std::vector<std::pair<int, int> > &points)
{
  double sum = 0;

  for(unsigned int i = 0; i < points.size(); i++)
  {
    sum += (points[i].first * points[i].second);
  }

  return sum;
}

int main(int argc, char **argv)
{

  if(argc == 2)
  {
    std::string line;
    std::ifstream in_file_obj(argv[1]);

    std::vector<std::pair<int, int> > data_points;

    std::getline(in_file_obj, line);
    std::stringstream input_conv(line);

    double input_val;

    input_conv >> input_val;
    std::cout << "independant variable: " << input_val << std::endl;

    while(std::getline(in_file_obj, line))
    {
      std::cout << line << std::endl;
      std::stringstream piece_stream(line);
      int x, y;
      piece_stream >> x;
      piece_stream >> y;

      std::cout << x << ", " << y << std::endl;

      data_points.push_back(std::make_pair(x, y));
    }

    std::cout << "number of pairs read in: " << data_points.size() << "\n";

    //calculate unknown coefficients from linear model
    std::cout << get_sum(data_points, 0, 1) << std::endl;
    std::cout << get_sum(data_points, 1, 1) << std::endl;
    std::cout << get_sum(data_points, 0, 2) << std::endl;
    std::cout << get_sum(data_points, 1, 2) << std::endl;

    unsigned int n = data_points.size();
    double slope = ((n * get_sum_of_products(data_points)) -
                  (get_sum(data_points, 0, 1) * get_sum(data_points, 1, 1))) /
                  ((n * get_sum(data_points, 0, 2)) -
                   (get_sum(data_points, 0, 1) * get_sum(data_points, 0, 1))),
           y_intercept =
                ((get_sum(data_points, 0, 2) * get_sum(data_points, 1, 1)) -
             (get_sum(data_points, 0, 1) * get_sum_of_products(data_points))) /
                  ((n * get_sum(data_points, 0, 2)) -
                   (get_sum(data_points, 0, 1) * get_sum(data_points, 0, 1)));

    std::cout << "slope: " << slope << std::endl;
    std::cout << "y-intercept: " << y_intercept << std::endl;

    double output_val = y_intercept + (slope * input_val);

    std::cout << "predicted response value: " << output_val << std::endl;
  }
  else
  {
    std::cerr << "Incorrect usage.  Try ./exec_name <input_filename>" << "\n";
  }
}


